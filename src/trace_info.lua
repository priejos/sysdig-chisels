--[[
Description:
   Chisel that prints out some information about the offline capture trace

Author:
   Jose M. Prieto
--]]

description = "Prints out some information about the captured trace"
short_description = "Trace information"
category = "Misc"

args = {}

evt_dt = nil

trace_info = {
    machinfo = nil,
    first = nil,
    last = nil,
    total = 0
}

function on_init()
    if sysdig.is_live() then
        -- this chisel only allows on offline captures
        print("Error: this chisel is only allowed on offline captures")
        return false
    end

    evt_dt = chisel.request_field("evt.datetime")
    
    return true
end

function on_capture_start()
    trace_info.machinfo = sysdig.get_machine_info()
    
    return true
end

function on_event()
    trace_info.total = evt.get_num()
    if trace_info.total == 1 then
        trace_info.first = evt.field(evt_dt)
    else
        trace_info.last = evt.field(evt_dt)
    end
end

function on_capture_end(ts_s, ts_ns, delta)
    -- if no events... do nothing
    if trace_info.total == 0 then
        return true
    end
    
    -- print information about machine    
    print("Trace information")
    print("   Hostname: ", trace_info.machinfo.hostname)
    print("   CPUs    : ", trace_info.machinfo.num_cpus)
    print("   RAM(MB) : ", trace_info.machinfo.memory_size_bytes/(1024*1024))
    
    -- print date and time of first event
    print("   Start   : ", trace_info.first)
    
    -- print date and time of last event
    print("   End     : ", trace_info.last)
    
    -- print total number of events
    print("   Events  : ", trace_info.total)
    
    return true
end

