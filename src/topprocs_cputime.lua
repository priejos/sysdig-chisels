--[[
Description:
   Chisel that computes the top most demanding processes in terms of CPU 
   scheduling time from a point of view of the OS. Both the overall and 
   per-core statistics are printed out.

Author:
   Jose M. Prieto
--]]

require "common"

--
-- chisel description
--
description = "Chisel that lists the top most processes (by default the top ten) with highest CPU time granted by the OS scheduler during the elapsed time of the capture"
short_description = "Top processes by scheduled CPU time"
category = "CPU Usage"

--
-- chisel argument table
--
args = {
    {
        name = "n_top", 
        description = "The most top <n>th processes (default: 10)", 
        argtype = "int",
        optional = true
    },
    {
        name = "only_proc_name",
        description = "Only take process name when searching for processes (default: false)",
        argtype = "string",
        optional = true
    }
}

--
-- global variables
--
param_ntop = 10         -- by default top ten
param_only_name = false -- by default false
cpu_count = 0           -- total number of CPU cores
cput = {}               -- table of cumulative cpu time keyed by per core id
proct = {}              -- table of cumulative cpu time keyed by proc.pid(proc.name)
cpuproct = {}           -- table of cumulative cpu time keyed by per core id
                        -- and proc.pid(proc.name)

--
-- chisel callbacks
--

-- argument notification
function on_set_arg(name, val)
    if name == "n_top" then
        param_ntop = tonumber(val) 
    end

    if name == "only_proc_name" then
        param_only_name = true
    end 

    return true
end

-- chisel initialization
function on_init()
    -- fields
    -- fnum = chisel.request_field("evt.num")
    fpid = chisel.request_field("proc.pid")
    fpname = chisel.request_field("proc.name")
    -- fcpu = chisel.request_field("evt.cpu")
    -- ftid = chisel.request_field("thread.tid")
    fexectime = chisel.request_field("thread.exectime")
    
    -- filter 
    chisel.set_filter("evt.type=switch")
    
    return true
end

-- just before capture starts
function on_capture_start()
    -- number of cpus
    cpu_count = sysdig.get_machine_info().num_cpus

    -- initialize exec time tables
    for i = 1, cpu_count do
        cput[i] = 0
        cpuproct[i] = {}
    end

    return true
end

-- event handler
function on_event()
    local pid = evt.field(fpid)
    local pname = evt.field(fpname)

    if pid == nil then
        return true
    end

    local pkey = ""
    if param_only_name then
        pkey = pname
    else
        pkey = string.format("%d(%s)", pid, pname) 
    end

    local cpuid = evt.get_cpuid() + 1
    local et = evt.field(fexectime)
    
    local incr_counter = function (v, d)
        if d == nil then
            d = 1
        end
        
        if v == nil then 
            return d
        else
            return v + d
        end
    end 
    
    -- update overall and per-core exec times
    cput["all"] = incr_counter(cput["all"], et)
    cput[cpuid] = incr_counter(cput[cpuid], et)
    
    -- update process exec times
    proct[pkey] = incr_counter(proct[pkey], et)
    
    -- update process exec time per core
    cpuproct[cpuid][pkey] = incr_counter(cpuproct[cpuid][pkey], et)
    
    return true
end

-- end of capture
function on_capture_end(ts_s, ts_ns, delta)
    local col_len = 25 
    local ntop = param_ntop

    local orderf = function(t,a,b) 
        return t[b] < t[a]
    end
    
    local print_timepct = function(t, d)
        if d == 0 then
            return "NaN"
        else
            return string.format("%.2f%%", t/d * 100)
        end
    end
    
    local print_row = function(...)
        local s = ""
        
        for i, v in ipairs {...} do
            if string.len(s) > 0 then
                s = s .. " "
            end 
            s = s .. extend_string(v, col_len)
        end
        
        print(s)
    end
    
    local print_header = function(...)
        local line = "" 
        
        for i in ipairs {...} do
            if string.len(line) > 0 then
                line = line .. " "
            end
            line = line .. string.rep("-", col_len)
        end
        
        print_row(...)
        print(line)
    end
    
    -- print totals
    print("Capture elapsed time: " .. format_time_interval(delta))
    print(string.format("Total scheduled CPU time (%d cores): %s",
              cpu_count,
              format_time_interval(cput["all"])))
    print("")
    
    -- print overall exec time stats per core
    print_header("Core", "Sched. CPU Time", "% of Elapsed")
    for i = 1, cpu_count do
        print_row("cpu"..(i-1), format_time_interval(cput[i]), print_timepct(cput[i], delta))
    end
    print("")
    
    -- print overall exec time stats per process
    print(string.format(">>> OVERALL (%d cores)", cpu_count))
    print_header("Process", "Sched. CPU Time", "% of Elapsed")
    for k, v in pairs_top_by_val(proct, ntop, orderf) do
        print_row(k, format_time_interval(v), print_timepct(v, delta*cpu_count))
    end
    print("")
    
    -- print process exec time stats per core
    for i = 1, cpu_count do
        print(string.format(">>> CORE %d",i-1))
        print_header("Process", "Sched. CPU Time", "% of Elapsed")
        for k, v in pairs_top_by_val(cpuproct[i], ntop, orderf) do
            print_row(k, format_time_interval(v), print_timepct(v, delta))
        end
        print("")
    end
    
    return true
end
